﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamScript : MonoBehaviour {
    public float moveBound;
    public float speed;

    private GameObject koala;
    private Vector3 kPos;

	// Use this for initialization
	void Start () {
        koala = GameObject.Find("Koala"); 
	}
	
	// Update is called once per frame
	void Update () {
        kPos = koala.transform.position;

        float toPlayer = kPos.y - transform.position.y; 
        if(toPlayer > moveBound)
        {
            transform.position += Vector3.up * Time.deltaTime * koala.GetComponent<PlayerMove>().speed;
        }
        else if (toPlayer < moveBound * -1)
        {
            transform.position -= Vector3.up * Time.deltaTime * koala.GetComponent<PlayerMove>().speed;
        }
		
	}
}
