﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardMove : MonoBehaviour {
    public float speed; 

    private Rigidbody2D rb;
    private GameObject koala;
    private Vector2 diff;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        koala = GameObject.Find("Koala");
        diff = transform.position - koala.transform.position;
        // if the asteroid is too parallel to the tree to be fair 
        if(Mathf.Abs(diff.x) < 2.5)
        {
            Destroy(gameObject); 
        }
        transform.rotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg);
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = -transform.right * speed;
	}
}
