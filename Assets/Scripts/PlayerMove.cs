﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grip
{
    public Vector2 pos;
    public int id;
    public bool active;

    public grip(Touch t)
    {
        pos = t.position;
        id = t.fingerId;
        active = true; 
    }

    public grip()
    {
        pos = Vector3.zero;
        id = 0;
        active = false;
    }
    public void release()
    {
        active = false;
    }
}

public class PlayerMove : MonoBehaviour {
    public float maxPull; 
    private grip[] paw = new grip[2];
    private Vector3 grippedPos;

    public float speed;

    private Camera cam;

	// Use this for initialization
	void Start () {
        paw[0] = new grip();
        paw[1] = new grip();
        cam = GameObject.Find("Camera").GetComponent<Camera>(); 
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.up * Time.deltaTime * speed; 
        }
        else if (!Input.GetKey(KeyCode.Space))
        {
            transform.position += Vector3.down * Time.deltaTime * speed; 
        }
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        for (int it = 0; it < paw.Length; it++)
                        {
                            if (!paw[it].active)
                            {
                                paw[it] = new grip(touch);
                                grippedPos = transform.position;
                                break;
                            } 
                        }
                        break;
                    case TouchPhase.Ended:
                        for (int it = 0; it < paw.Length; it++)
                        {
                            if (paw[it].id == touch.fingerId)
                            {
                                paw[it].release();
                                break;
                            }
                        }
                        break; 
                    case TouchPhase.Moved:
                        if(Input.touchCount == 1)
                        {
                            for (int it = 0; it < paw.Length; it++)
                            {
                                if (paw[it].id == touch.fingerId)
                                {
                                    float disp = cam.ScreenToWorldPoint(paw[it].pos).y - cam.ScreenToWorldPoint(touch.position).y;
                                    Debug.Log(disp);
                                    if (Mathf.Abs(disp) < maxPull)
                                    {
                                        transform.position = grippedPos + Vector3.up * disp;
                                        
                                    }
                                    break;
                                }
                            }
                        }
                        break;

                }
            }
        }
	}
}
