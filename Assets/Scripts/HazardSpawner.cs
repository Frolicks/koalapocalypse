﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardSpawner : MonoBehaviour
{

    public GameObject pterodactyl;
    public int AssSpawnRate; 

    private GameObject koala;

    // Use this for initialization
    void Start()
    {
        koala = GameObject.Find("Koala");
        Invoke("spawn", AssSpawnRate);

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void spawn()
    {
        Invoke("spawn", AssSpawnRate);
        Instantiate(pterodactyl, koala.transform.position + ((Vector3)(Random.insideUnitCircle)) * 20, Quaternion.identity);
    }
}